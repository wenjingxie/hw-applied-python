#data generation
import numpy as np
import scipy.misc as misc
import scipy.ndimage as ndimage
import matplotlib
matplotlib.use('TKAgg')
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from glob import glob
#Initialize
ndim=256  #number of pixels in the image
l=ndim/4  #triangle limit
#Im=np.zeros((ndim,ndim))
N=100   #number of steps
#Generate Vertices
def GenerateVertices():
    vertices=[]
    vertices.append([ndim/2,np.random.random_integers(5*ndim/8,ndim-l)])   #vertice on y+ axis/Vertice A
    vertices.append([np.random.random_integers(5*ndim/8,ndim-l),np.random.random_integers(l,3*ndim/8)])     #vertice in fourth/Vertice B
    vertices.append([np.random.random_integers(l,3*ndim/8),np.random.random_integers(l,3*ndim/8)])          #vertice in thrird/Vertice C
    return vertices
#triangle_vertices=GenerateVertices()
#print triangle_vertices
#whether point is inside the triangle
def IsInside(point,vertices):
    AB=[vertices[1][0]-vertices[0][0],vertices[1][1]-vertices[0][1]]
    BC=[vertices[2][0]-vertices[1][0],vertices[2][1]-vertices[1][1]]
    CA=[vertices[0][0]-vertices[2][0],vertices[0][1]-vertices[2][1]]
    OB=[vertices[1][0]-point[0],vertices[1][1]-point[1]]
    OC=[vertices[2][0]-point[0],vertices[2][1]-point[1]]
    OA=[vertices[0][0]-point[0],vertices[0][1]-point[1]]
    Bp=np.cross(AB,OB)
    Cp=np.cross(BC,OC)
    Ap=np.cross(CA,OA)
    #return AB,BC,CA,OB,OC,OA
    if ((Bp>=0) & (Cp>=0) &( Ap>=0)):
        return True
    else:
        return False
#Generate Random Triangle
def GenerateRandomTriangle():
    Im=np.zeros((ndim,ndim))
    Random_vertices=GenerateVertices()
    for ii in range(0,ndim):
        for jj in range(0,ndim):
            if IsInside([ii,jj],Random_vertices):
                Im[ii,jj]=1
            else:
                Im[ii,jj]=0
    plt.imshow(Im,cmap=plt.cm.gray)
    plt.show()
    # misc.imsave('triangle.png',Im)
    return Im
# GenerateRandomTriangle()
  
#generate triangle
# def GenerateTriangle(Vertices):
#   Im=np.zeros((ndim,ndim))
#   for ii in range(0,ndim):
#       for jj in range(0,ndim):
#           if IsInside([ii,jj],Vertices):
#               Im[ii,jj]=1
#           else:
#               Im[ii,jj]=0
#   plt.imshow(Im,cmap=plt.cm.gray)
#   plt.show()
#   return Im
    # misc.imsave('triangle',Im)
#def rotation matrix
def Rotation_Matrix(theta):
    return [[np.cos(theta),-np.sin(theta)],[np.sin(theta),np.cos(theta)]]
# rotation
def Spin_Save():
    Rotation_Speed=10
    deltaT=0.2
    deltaTheta=deltaT*Rotation_Speed
    Im=GenerateRandomTriangle()
    for time in range(N):
        theta=time*deltaTheta
        new_image=ndimage.rotate(Im,theta,reshape=False)
        misc.imsave('picture/triangle_%d.png'%time,new_image)
    filelist=glob('triangle*.png')
    filelist.sort()
# get image files
def GetImageFile(Number):
    im=misc.imread('picture/triangle_%d.png'%Number,True)
    return im
        # plt.imshow(new_image,cmap=plt.cm.gray)
        # plt.show()
        # old_coordinate=Random_Vertices[:]
        # print 'first ran', Random_Vertices
        # print 'first',old_coordinate
        # for vertice in old_coordinate:
        #   vertice[0]=vertice[0]-center[0]
        #   vertice[1]=vertice[1]-center[1]
        # coordiante=np.transpose(old_coordinate)
        # print 'second',old_coordinate
        # print 'second ran' ,Random_Vertices
        # New_Coordinate=np.transpose(np.dot(Rotation_Matrix(theta),coordiante))
        # for vertice in New_Coordinate:
        #   vertice[0]=vertice[0]+center[0]
        #   vertice[1]=vertice[1]+center[1]
        # GenerateTriangle(New_Coordinate)
  
# plt.imshow(GetImageFile(44))
# plt.show()
#animation method 1
# def Image_Animation(N):
#   plt.ion()
#   im_list=[]
#   # plt.xlim([0,ndim])
#   # plt.ylim([0,ndim])
#   for ii in range (N):
#       plt.imshow(GetImageFile(ii),cmap=plt.cm.gray)
#       plt.draw()
#animation method 2
def Image_Animation(Number):
    fig = plt.figure()
    im_list=[]
    for ii in range (Number):
        im=plt.imshow(GetImageFile(ii))
        im_list.append([im])
    anim=animation.ArtistAnimation(fig, im_list, interval=50, blit=False)
    # anim.save('rotation_triangle.mp4')
    plt.show()
    
  
#Feature Identification
#Generate "binary_vertex_image"  can only be used on non-rotationary image
def GetBinaryVertexImage(triangle_image):
    Im=GetImageFile(triangle_image)
    plt.imshow(Im,cmap=plt.cm.gray)
    plt.show()
    New_Im=ndimage.binary_opening(Im, structure=np.ones((3,3))).astype(np.uint8)
    plt.imshow(New_Im,cmap=plt.cm.gray)
    plt.show()
    plt.imshow(np.logical_xor(Im,New_Im),cmap=plt.cm.gray)
    plt.show()
# Spin_Save()
# Image_Animation(N)
# GetBinaryVertexImage(0)
#Generate harris vertex image
def GetHarrisVertexImage(triangle_image, kappa=0.05, windowSize=9, windowSpace=1, sigma=2):
    Im=GetImageFile(triangle_image)[:]
    # Im=GenerateRandomTriangle()
    Imx=ndimage.sobel(Im,axis=0)
    Imy=ndimage.sobel(Im,axis=1)
    #generate gaussian matrix
    N_kernel=windowSize
    gw=np.zeros((N_kernel,N_kernel))
    center=(N_kernel-1)/2
    gw[center,center]=1
    gw=ndimage.gaussian_filter(gw,sigma)
    harrisI=np.zeros(Im.shape)
    for ix in range(center, Im.shape[0]-center,windowSpace):
        for iy in range(center,Im.shape[1]-center,windowSpace):
            Imx_windows=(Imx[ix-center:ix-center+N_kernel,iy-center:iy-center+N_kernel])
            Imx2=np.multiply(Imx_windows,Imx_windows)
            Imx2average=np.sum(np.multiply(Imx2,gw))
            Imy_windows=(Imy[ix-center:ix-center+N_kernel,iy-center:iy-center+N_kernel])
            Imy2=np.multiply(Imy_windows,Imy_windows)
            Imy2average=np.sum(np.multiply(Imy2,gw))
            Imxy=np.multiply(Imx_windows,Imy_windows)
            Imxyaverage=np.sum(np.multiply(Imxy,gw))
            M=[[Imx2average,Imxyaverage],[Imxyaverage,Imy2average]]
            harrisI[ix,iy]=np.linalg.det(M)-kappa*np.trace(M)**2
    mask_edge=harrisI<0
    # plt.imshow(mask_edge,cmap=plt.cm.gray)
    # plt.show()
    corner_threshold=max(harrisI.ravel()) * 0.2
    mask_corner=(harrisI>corner_threshold)
    label_im, nb_labels = ndimage.label(mask_corner)
    # print np.where(label_im==1)
    # plt.imshow(mask_corner,cmap=plt.cm.gray)
    # plt.show()
    # plt.imshow(label_im,cmap=plt.cm.spectral)
    # plt.show()
    return label_im
# GetHarrisVertexImage(0,0.1)
# Get exact coordinate of vertices
def GetCoordinateVertices(label_image):
    label_im=label_image
    mask_label1=np.equal(label_im,1)
    x1=int(ndimage.measurements.center_of_mass(mask_label1)[0])
    y1=int(ndimage.measurements.center_of_mass(mask_label1)[1])
    mask_label2=np.equal(label_im,2)
    x2=int(ndimage.measurements.center_of_mass(mask_label2)[0])
    y2=int(ndimage.measurements.center_of_mass(mask_label2)[1])
    mask_label3=np.equal(label_im,3)
    x3=int(ndimage.measurements.center_of_mass(mask_label3)[0])
    y3=int(ndimage.measurements.center_of_mass(mask_label3)[1])
    return [(x1,y1),(x2,y2),(x3,y3)]
# GetCoordinateVertices(GetHarrisVertexImage(0,0.1))
#Get Lucas Kanade Flow Fields
def GetLucasKanadeFlowFields(image1, image2, dt, kappa=0.05,windowSize=9, windowSpace=2, sigma=2):
    Im1=GetImageFile(image1)
    Im2=GetImageFile(image2)
    Im=Im1[:]
    # Im=GenerateRandomTriangle()
    Imx=ndimage.sobel(Im,axis=0)
    Imy=ndimage.sobel(Im,axis=1)
    Imt=np.multiply((Im1-Im2),1/dt)
    #generate gaussian matrix
    N_kernel=windowSize
    gw=np.zeros((N_kernel,N_kernel))
    center=(N_kernel-1)/2
    gw[center,center]=1
    gw=ndimage.gaussian_filter(gw,sigma)
    harrisI=np.zeros(Im.shape)
    Velocityx=np.zeros(Im.shape)
    Velocityy=np.zeros(Im.shape)
    for ix in range(center, Im.shape[0]-center,windowSpace):
        for iy in range(center,Im.shape[1]-center,windowSpace):
            Imx_windows=(Imx[ix-center:ix-center+N_kernel,iy-center:iy-center+N_kernel])
            Imx2=np.multiply(Imx_windows,Imx_windows)
            Imx2average=np.sum(np.multiply(Imx2,gw))
            Imy_windows=(Imy[ix-center:ix-center+N_kernel,iy-center:iy-center+N_kernel])
            Imy2=np.multiply(Imy_windows,Imy_windows)
            Imy2average=np.sum(np.multiply(Imy2,gw))
            Imxy=np.multiply(Imx_windows,Imy_windows)
            Imxyaverage=np.sum(np.multiply(Imxy,gw))
            M=[[Imx2average,Imxyaverage],[Imxyaverage,Imy2average]]
            harrisI[ix,iy]=np.linalg.det(M)-kappa*np.trace(M)**2
            Imt_windows=(Imt[ix-center:ix-center+N_kernel,iy-center:iy-center+N_kernel])
            ImxImt=np.multiply(Imt_windows,Imx_windows)
            ImyImt=np.multiply(Imt_windows,Imy_windows)
            ImxImtaverage=np.sum(np.multiply(ImxImt,gw))
            ImyImtaverage=np.sum(np.multiply(ImyImt,gw))
            Imtaverage=np.array([[ImxImtaverage],[ImyImtaverage]])
            if np.linalg.det(M)==0:
                Velocityx[ix,iy]=0
                Velocityy[ix,iy]=0
            else:         
                Velocity = np.dot(np.linalg.inv(M),Imtaverage)
                Velocityx[ix,iy]=Velocity[0,0]
                Velocityy[ix,iy]=Velocity[1,0]
    plt.imshow(Velocityx)
    misc.imsave('Velocityx.png',Velocityx)
    plt.show()
    plt.imshow(Velocityy)
    misc.imsave('Velocityy.png',Velocityy)
    plt.show()
GetLucasKanadeFlowFields(0, 1, 0.2)
# plot corner coordinates
def PlotCornerCoordinate():
    x1list=[]
    y1list=[]
    x2list=[]
    y2list=[]
    x3list=[]
    y3list=[]
    deltaT=0.2
    Timelist=[]
    for kk in range(N/4):   # N global viriable number of images
        label_image=GetHarrisVertexImage(kk,0.1)
        x1list.append(GetCoordinateVertices(label_image)[0][0])
        y1list.append(GetCoordinateVertices(label_image)[0][1])
        x2list.append(GetCoordinateVertices(label_image)[1][0])
        y2list.append(GetCoordinateVertices(label_image)[1][1])
        x3list.append(GetCoordinateVertices(label_image)[2][0])
        y3list.append(GetCoordinateVertices(label_image)[2][1])
        Timelist.append(deltaT*kk)
    plt.subplot(331)
    plt.plot(Timelist,x1list)
    plt.subplot(332)
    plt.plot(Timelist,y1list)
    plt.subplot(333)
    pls.plot(x1list,y1list)
    plt.subplot(334)
    plt.plot(Timelist,x2list)
    plt.subplot(335)
    plt.plot(Timelist,y2list)
    plt.subplot(336)
    plt.plot(x2list,y2list)
    plt.subplot(337)
    plt.plot(Timelist,x3list)
    plt.subplot(338)
    plt.plot(Timelist,y3list)
    plt.subplot(339)
    plt.plot(x3list,y3list)
    plt.show()
# PlotCornerCoordinate()